package com.reactnative.homework.nativemodules

import android.preference.PreferenceManager

import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod

class MyStorageModule(context : ReactApplicationContext) : ReactContextBaseJavaModule(context) {

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    override fun getName(): String {
        return "MyStorage"
    }

    /**
     * Sets value for key
     */
    @ReactMethod
    fun setItem(key: String, value: String, promise: Promise) {
        val isSuccess = prefs.edit().putString(key, value)
                .commit()
        promise.resolve(isSuccess)
    }

    /**
     * Returns the value of specified key
     */
    @ReactMethod
    fun getItem(key: String, promise: Promise) {
        val result = prefs.getString(key, null)
        promise.resolve(result)
    }

    /**
     * Clears all cached key-value pairs
     */
    @ReactMethod
    fun clear(promise: Promise) {
        val isSuccess = prefs.edit()
                .clear()
                .commit()
        promise.resolve(isSuccess)
    }

}