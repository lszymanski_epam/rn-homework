package com.reactnative.homework.nativemodules


import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.reactnative.homework.MainActivity

class NotificationsModule(
        private val reactContext: ReactApplicationContext
) : ReactContextBaseJavaModule(reactContext) {

    private val channelId = "com.reactnative.homework"

    override fun getName(): String {
        return "Notifications"
    }

    @ReactMethod
    fun initModule() {
        createNotificationChannel(
            channelId,
            "Notifications for React Native homework",
            "The channel contains notifications from React Native js code")
    }

    @ReactMethod
    fun showNotification(title: String, message: String, callback: Callback) {
        val mBuilder = NotificationCompat.Builder(reactContext, channelId)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(createPendingIntent())
                .setAutoCancel(true)

        val notificationId = 1
        with(NotificationManagerCompat.from(reactContext)) {
            // notificationId is a unique int for each notification that you must define
            notify(notificationId, mBuilder.build())
        }

        callback.invoke(notificationId)
    }

    private fun createNotificationChannel(
            id: String, name: String,
            description: String
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(id, name, importance).apply {
                this.description = description
                enableVibration(true)
            }
            val notificationManager: NotificationManager =
                    reactContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createPendingIntent() : PendingIntent {
        val resultIntent = Intent(reactApplicationContext, MainActivity::class.java)
        return PendingIntent.getActivity(
                reactApplicationContext,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )
    }
}