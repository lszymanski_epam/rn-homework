//
//  RCTEventEmitter+NotificationsModule.h
//  homework
//
//  Created by Lukasz Szymanski on 11.02.2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "React/RCTEventEmitter.h"
#import <React/RCTBridgeModule.h>


@interface NotificationsModule : RCTEventEmitter

RCT_EXPORT_METHOD(initModule)
{
  NSLog(@"[NotificationsModule] initModule");
}


RCT_EXPORT_METHOD(showNotification:(NSString*)title
                  andText:(NSString *)text
                  andCallback:(RCTResponseSenderBlock)block)
{
  [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
    [self createAlarm:1 title:title text:text];
    NSArray* events = @[@"1"];
    block(@[[NSNull null], events]);
  }];
};

- (void)createAlarm:(int)id title:(NSString*) title text:(NSString *)text {
  UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
  content.title = [NSString localizedUserNotificationStringForKey:title arguments:nil];
  content.body = [NSString localizedUserNotificationStringForKey:text
                                                       arguments:nil];
  content.sound = [UNNotificationSound defaultSound];
  UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                triggerWithTimeInterval:5 repeats:NO];
  UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@“1”
                                                                        content:content trigger:trigger];
  UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
  [center addNotificationRequest:request withCompletionHandler:nil];
}

@end

