import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { LoginScreen } from './screens/auth'
import { ProductsListScreen, ProductScreen } from './screens/products'
import { navBarStyle, sentry } from './styles'
import { Sentry, SentryLog } from 'react-native-sentry';

Sentry.config(
  sentry.SENTRY_DSN, {
  logLevel: SentryLog.Debug,
  deactivateStacktraceMerging: false
}).install();

const AppNavigator = createStackNavigator(
  {
    Login: { screen: LoginScreen },
    ProductsList: { screen: ProductsListScreen },
    Product: { screen: ProductScreen }
  },
  {
    initialRouteName: 'ProductsList',
    defaultNavigationOptions: navBarStyle
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
  render() {
    return (
      <AppContainer />
    );
  }
}