import React from 'react';
import {
  Text,
  ActivityIndicator,
  TouchableOpacity,
  ViewPropTypes
} from 'react-native';
import PropTypes from 'prop-types';
import { colors } from '../styles'

export const LoadableAppButton = ({ title, onPress, containerStyle, textStyle, isLoading }) => {
  return (
    <TouchableOpacity
      activeOpacity = {0.8}
      onPress = { onPress }
      style = { containerStyle }>
      { 
        isLoading ? 
        <ActivityIndicator size="small" color={ colors.textSecondary } />
        :
        <Text style = { textStyle }>
            { title }
        </Text>
      }
    </TouchableOpacity>
  );
}

LoadableAppButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  containerStyle: ViewPropTypes.style,
  textStyle: Text.propTypes.style
};

LoadableAppButton.defaultProps = {
  containerStyle: { },
  textStyle: { }
};

