import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { styles, typography, paddings } from '../styles'


export const ProductItemView = ({ iconSource, productName, productSku, onPressAction }) => {
  return (
    <View style={localStyles.containerProductItem}>
      <Image
        style={localStyles.icon}
        source={iconSource}/>
      <Text 
        style={localStyles.title}>
        {productName}
      </Text>
      <TouchableOpacity 
        activeOpacity = {0.8}
        onPress={ () => onPressAction(iconSource, productName, productSku) }>
        <Image
          style={ localStyles.image }
          source={ require('../../assets/drawables/chevron_right.png') } />
      </TouchableOpacity>
    </View>
  );
}

ProductItemView.propTypes = {
  productName: PropTypes.string.isRequired
};

ProductItemView.defaultProps = {
  onPressAction: () => { },
};

const localStyles = {
  containerProductItem: {...styles.containerProductItem },
  title: {...typography.textRegular, flex: 1, textAlign: 'left', marginLeft: paddings.standard },
  icon: {...styles.logo},
  image: {...styles.image}
};