import React, { Component } from 'react';
import { View, Animated, Easing, Text, Image, TouchableOpacity, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { styles, typography, paddings, dimens, colors } from '../styles'

const openIcon = require('../../assets/drawables/menu.png')
const closeIcon = require('../../assets/drawables/arrow_back.png')

export class HeaderView extends Component {

    constructor(props) {
        super(props)
        
        this.state = {
            title: this.props.title,
            spinValue: new Animated.Value(0),
            isMenuDrawerOpened: false,
            currentIcon: openIcon
        }

        this.onHamburgerPressAction = this.props.onHamburgerPressAction
    }

    _onPressAction = () => {
        const isOpened = this.state.isMenuDrawerOpened
        this._animateButton(isOpened ? 0 : 1)
        this.onHamburgerPressAction()
    }

    _animateButton = (toValue) => {
        Animated.timing(this.state.spinValue, {
            toValue: 0.5,
            duration: 150,
            easing: Easing.linear,
            useNativeDriver: true,
        }).start(() => {
            this._setNewState()

            Animated.timing(this.state.spinValue, {
                toValue,
                duration: 150,
                easing: Easing.linear,
                useNativeDriver: true,
            }).start();
        });
    }

    _setNewState = () => {
        const newIsMenuDrawerOpened = !this.state.isMenuDrawerOpened
        this.setState({ 
            isMenuDrawerOpened: newIsMenuDrawerOpened, 
            currentIcon: newIsMenuDrawerOpened ? closeIcon : openIcon 
        });
    }

    render() {
        const spin = this.state.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '180deg'],
        });

        return (
            <View 
                style={ localStyles.containerHeaderView }>
                <Animated.View 
                    style={ { transform: [{ rotate: spin }] } }>
                    <TouchableOpacity
                    activeOpacity={ 0.8 }
                    onPress={ () => this._onPressAction() }>
                    <Image
                        style={ localStyles.menuDrawerIcon }
                        source={ this.state.currentIcon } />
                    </TouchableOpacity>
                </Animated.View>
                <Text
                    style={ typography.textNavBar }> 
                    {this.state.title} 
                </Text>
            </View>
        )
    }
}

const localStyles = {
  containerHeaderView: {
    flexDirection: "row",
    height: dimens.navbarHeight,
    backgroundColor: colors.backgroundTertiary,
    marginTop: Platform.OS == "ios" ? 20 : 0 // only for IOS to give StatusBar Space
  },
  menuDrawerIcon: {
      ...styles.logo,
      alignSelf: "center",
      marginTop: paddings.micro,
      marginBottom: paddings.micro,
      marginLeft: paddings.micro,
      marginRight: paddings.micro
  }
};