import React from 'react';
import { Modal, View, Text } from 'react-native';
import { AppButton } from '../components'
import PropTypes from 'prop-types';
import { styles, typography, paddings } from '../styles'


export const CancelableActionModal = ({ visible, title, message, primaryButtonText, onPrimaryActionPress, onCancelActionPress }) => {
  return (
    <Modal
      animationType="slide"
      transparent={ false }
      visible={ visible }
      onRequestClose={ () => onCancelActionPress() } >
      <View style={ styles.containerCancelableActionModal }>
        <Text 
          style={ typography.textModalTitle }>
          { title }
        </Text>
        <Text 
          style={ typography.textModalMessage } >
          { message }
        </Text>
        <View style={ localStyles.containerButtons }>
          <AppButton
            title='Cancel'
            onPress={ () => onCancelActionPress() }
            containerStyle={ localStyles.containerLeftButton }
            textStyle={ typography.button }/>
          <AppButton
            title={ primaryButtonText }
            onPress={ () => onPrimaryActionPress() }
            containerStyle={ styles.buttonContainer }
            textStyle={ typography.button }/>
        </View>
      </View>
    </Modal>
  );
}

CancelableActionModal.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
};

CancelableActionModal.defaultProps = {
  visible: false,
  primaryButtonText: 'OK',
  onPrimaryActionPress: () => { },
  onPrimaryActionPress: () => { },
};

const localStyles = {
  containerButtons: { ...styles.containerHorizontal, marginTop: paddings.medium },
  containerLeftButton: { ...styles.buttonContainer, marginRight: paddings.medium }
};