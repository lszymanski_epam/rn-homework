import React, { Component } from 'react';
import { View, ScrollView, Text, Image, LayoutAnimation, Platform, UIManager, Vibration } from 'react-native';
import { styles, colors, typography, paddings, storageKeys, sentry, VIBRATION_PATTERN } from '../../styles'
import { loginRequest } from '../../api'
import { LoadableAppButton, AppTextInput, CancelableActionModal } from '../../components'
import LottieView from 'lottie-react-native';
import DeviceInfo from 'react-native-device-info';
import { Sentry, SentrySeverity } from 'react-native-sentry';

import { MySecureStorage } from '../../nativemodules'

export class LoginScreen extends Component {

  static navigationOptions = {
    title: 'Login',
  };

  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      isRequestLoading: false,
      isModalVisible: false,
      errorMessage: '',
      isEmptyUsernameError: false,
      isEmptyPasswordError: false
    }

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  _loginRequest = (username, password) => {
    let isError = this._handleEmptyFieldsErrors(username, password)

    if (isError)
      return

    this._showLoadingIndicator(true)

    loginRequest(username, password)
      .then((response) => {
        this._showLoadingIndicator(false)
        if (response.errorMessage === null) {
          const token = response.result
          try {
            this._sentryLoginSuccess(username)
            this._storeToken(token)
            this._goToProductsList(token)
          } catch (error) {
            Sentry.captureException(error)
            this._showErrorModal("Problem while caching token!")
          }
        } else {
          this._showErrorModal(response.errorMessage)
        }
      })
  }

  _handleEmptyFieldsErrors(username, password) {
    var isError = false
    if (!username || username.length === 0) {
      this._showUsernameEmptyFieldError(true)
      isError = true
    } else {
      this._showUsernameEmptyFieldError(false)
    }

    if (!password || password.length === 0) {
      this._showPasswordEmptyFieldError(true)
      isError = true
    } else {
      this._showPasswordEmptyFieldError(false)
    }

    if (isError) {
      Vibration.vibrate(VIBRATION_PATTERN)
    }

    return isError
  }

  _showUsernameEmptyFieldError(shouldShow) {
    LayoutAnimation.configureNext(shouldShow ? LayoutAnimation.Presets.spring : LayoutAnimation.Presets.linear)
    this.setState({
      isEmptyUsernameError: shouldShow
    })
  }

  _showPasswordEmptyFieldError(shouldShow) {
    LayoutAnimation.configureNext(shouldShow ? LayoutAnimation.Presets.spring : LayoutAnimation.Presets.linear)
    this.setState({
      isEmptyPasswordError: shouldShow
    })
  }

  _getEmptyFieldErrorStyle(state) {
    return state ? localStyles.errorShownEmptyField : localStyles.errorHiddenEmptyField
  }

  _sentryLoginSuccess = (username) => {
    Sentry.setUserContext({
      email: username
    });
    Sentry.captureMessage(sentry.MESSAGE_LOGGED_IN, {
      level: SentrySeverity.Debug
    });
  }

  _storeToken = async (token) => {
    console.log('token: ' + token)
    // await MyStorage.setItem(storageKeys.TOKEN, token)
    await MySecureStorage.setToken(token)
  }

  _showLoadingIndicator = (shouldShow) => {
    this.setState({
      isRequestLoading: shouldShow
    })
  }

  _goToProductsList(token) {
    this.props.navigation.navigate('ProductsList', { token: token })
  }

  _showErrorModal(message) {
    this.setState({
      errorMessage: message, 
      isModalVisible: true,
    })
    
    Vibration.vibrate(VIBRATION_PATTERN)
  }

  _hideErrorModal() {
    this.setState({
      isModalVisible: false
    })
  }

  _onModalTryAgainActionPress() {
    this._loginRequest(this.state.username, this.state.password)
    this._hideErrorModal()
  }
  
  _onModalCancelActionPress() {
    this._hideErrorModal()
  }

  render() {
    return (
      <ScrollView contentContainerStyle={ styles.container }>
      <View style={ styles.containerCenter }>
        <View style={ localStyles.containerRow }>
          <Image
            style={ localStyles.logo }
            source={ require('../../../assets/drawables/face_happy.png') }
          />
          <LottieView
            style={ localStyles.animation }
            source={require('../../../assets/animations/jumpy-heart.json')}
            autoPlay
            loop
          />
        </View>
      </View>
        <Text style={ localStyles.header }>
          Hello { DeviceInfo.getSystemName() }!
        </Text>
        <AppTextInput 
          style={ localStyles.email } 
          placeholder='email'
          keyboardType='email-address'
          onChangeText={ (value) => this.setState({username: value}) }
          value={ this.state.username }/>
        <Text style={ [ this.state.isEmptyUsernameError ? localStyles.errorShownEmptyField : localStyles.errorHiddenEmptyField ] }>
          Email field is empty!
        </Text>
        <AppTextInput 
          style={ localStyles.password }
          placeholder='password'
          secureTextEntry={ true }
          onChangeText={ (value) => this.setState({ password: value }) }
          value={ this.state.password }/>
        <Text style={ [ this.state.isEmptyPasswordError ? localStyles.errorShownEmptyField : localStyles.errorHiddenEmptyField ] }>
          Password field is empty!
        </Text>
        <View style={ styles.containerCenter }>
          <LoadableAppButton
            title='login'
            onPress={ () => this._loginRequest(this.state.username, this.state.password) }
            containerStyle={ styles.buttonContainer }
            textStyle={ typography.button }
            isLoading={ this.state.isRequestLoading } />
        </View>
        <CancelableActionModal
          visible={ this.state.isModalVisible }
          title='Error'
          message= { this.state.errorMessage }
          primaryButtonText='Try again'
          onPrimaryActionPress= { () => this._onModalTryAgainActionPress() }
          onCancelActionPress= { () => this._onModalCancelActionPress() }
        />
      </ScrollView>
    )
  }
};

export default LoginScreen;

const localStyles = {
  containerRow: { ...styles.containerHorizontal, backgroundColor: colors.backgroundPrimary },
  logo: { ...styles.logo, marginTop: paddings.xlarge },
  animation: { ...styles.animation },
  header: { ...typography.textHeader, marginBottom: paddings.large },
  email: { ...typography.textRegular },
  password: { ...typography.textRegular },
  errorShownEmptyField: { ...typography.textSmall, alignSelf: 'flex-start', marginBottom: paddings.small },
  errorHiddenEmptyField: { ...typography.textSmall, alignSelf: 'flex-start', marginBottom: paddings.small, width: 0, height: 0 }
};
