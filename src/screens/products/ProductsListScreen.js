import React, { Component } from 'react';
import { Animated, View, FlatList, BackHandler, Vibration } from 'react-native';
import { styles, colors, dimens, storageKeys, VIBRATION_PATTERN, sentry } from '../../styles'
import { ProductItemView, CancelableActionModal, HeaderView } from '../../components'
import { productsListRequest } from '../../api'
import SplashScreen from 'react-native-splash-screen'
import { Sentry } from 'react-native-sentry';

import { MySecureStorage } from '../../nativemodules'

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

export class ProductsListScreen extends Component {

  static navigationOptions = {
    header: null
  };
  
  scrollY = new Animated.Value(0);
  headerY;

  constructor(props) {
    super(props)
    this.state = {
      token: null,
      errorMessage: '',
      isModalVisible: false,
      isRefreshing: false, 
      productsList: [],
      pageSize: 15,
      currentPage: 1,
      isFullListLoaded: false
    }
    this.headerY = Animated.multiply(Animated.diffClamp(this.scrollY, 0, dimens.navbarHeight), -1)
    console.log("token = " + this.state.token)
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    this.subscription = this.props.navigation.addListener('willFocus', () => this._loadTokenFromStorage())
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    this.subscription.remove()
  }

  onBackPress = () => {
    BackHandler.exitApp()
    return true;
  }

  _loadTokenFromStorage = async () => {
    try {
      // const token = await MyStorage.getItem(storageKeys.TOKEN);
      const token = await MySecureStorage.getToken();
      this.setState({ 
        token: token,
      })
      if (token !== null && token != false) {
        this._loadProducts(this.state.pageSize, this.state.currentPage)
      } else {
        this._gotoLoginScreen()
      }
    } catch (error) {
      Sentry.captureException(error)
      this._gotoLoginScreen()
    }
    SplashScreen.hide();
  }

  _clearCache = () => {
    MySecureStorage.clear()
      .then(console.warn("Cache cleared."))
      .then(this.setState({ token: null }))
      .catch( error => {
        Sentry.captureException(error)
        console.warn("Cache not cleared.")
      })
  }

  _gotoLoginScreen = () => {
    this.props.navigation.navigate('Login')
  }

  _loadProducts = (pageSize, currentPage) => {
    if (this.state.isFullListLoaded) {
      return
    }

    productsListRequest(pageSize, currentPage)
      .then((response) => {
        if (response.errorMessage === null) {
          const loadedProducts = response.result.items
          const updatedList = [ ...this.state.productsList, ...loadedProducts ]
          const isFullList = updatedList.length >= response.result.total_count
          console.log(loadedProducts)
          this.setState({ 
            productsList: updatedList,
            currentPage: isFullList ? this.state.currentPage : this.state.currentPage + 1,
            isFullListLoaded: isFullList
          })
        } else {
          this._showErrorModal(response.errorMessage)
        }
      })
  }

  _onRefreshProductsList = () => {
    this.setState({
      isRefreshing: true
    })

    const itemsToFetch = Math.max(this.state.productsList.length, this.state.pageSize)
    productsListRequest(itemsToFetch, 1)
      .then((response) => {
        if (response.errorMessage !== null) {
          this._showErrorModal(response.errorMessage)
        }
    
        this.setState({
          productsList: response.errorMessage !== null ? response.result.items : this.state.productsList,
          isRefreshing: false
        })
      })
  }

  _onEndListReached = () => {
    this._loadProducts(this.state.pageSize, this.state.currentPage)
  }
  
  _onListItemPress = (iconSource, productName, productSku) => {
    this._sentryItemSelected(productName, productSku)

    this.props.navigation.navigate('Product', { iconSource: iconSource, productName: productName, productSku: productSku, token: this.state.token })
  }

  _sentryItemSelected = (productName, productSku) => {
    // for test purposes only
    // Sentry.nativeCrash()
    // Sentry.captureException(Error("Custom test error"))

    Sentry.captureBreadcrumb({
      message: sentry.PRODUCT_SELECTED,
      category: sentry.CATEGORY_ACTION,
      data: {
        productName: productName,
        productSku: productSku
      }
    });
  }

  _showErrorModal(message) {
    this.setState({
      errorMessage: message, 
      isModalVisible: true
    })

    Vibration.vibrate(VIBRATION_PATTERN)
  }

  _hideErrorModal() {
    this.setState({
      isModalVisible: false
    })
  }

  _onModalTryAgainActionPress = () => {
    this._loadProducts(this.state.pageSize, this.state.currentPage)
    this._hideErrorModal()
  }
  
  _onModalCancelActionPress = () => {
    this._hideErrorModal()
  }

  _keyExtractor = (item, index) => item.id.toString()

  _renderItem = ({item}) => (
    <View style={ styles.container } >
      <ProductItemView 
        iconSource={ require('../../../assets/drawables/map.png') }
        productName={ item.name }
        productSku={ item.sku }
        onPressAction={ (iconSource, productName, productSku) => this._onListItemPress(iconSource, productName, productSku) }
      />
    </View>
  );
  
  render() {
    return (
      <View 
        style={ localStyles.mainContainer } >
        <Animated.View
          style={{ transform: [{ translateY: this.headerY }] }}>
          <HeaderView 
            title='Products'
            onHamburgerPressAction={ () => this._clearCache() }
          />
        </Animated.View>

        <AnimatedFlatList
          onScroll={ 
            Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.scrollY }}}],
              { useNativeDriver: true },
            )
          }
          data={ this.state.productsList }
          keyExtractor={ this._keyExtractor }
          renderItem={ this._renderItem }
          refreshing={ this.state.isRefreshing }
          onRefresh={ () => this._onRefreshProductsList() }
          onEndReachedThreshold={ 0.01 }
          onEndReached={ () => this._onEndListReached() }
        />

        <CancelableActionModal
          visible={ this.state.isModalVisible }
          title='Error'
          message= { this.state.errorMessage }
          primaryButtonText='Try again'
          onPrimaryActionPress= { () => this._onModalTryAgainActionPress() }
          onCancelActionPress= { () => this._onModalCancelActionPress() }
        />
      </View>
    )
  }
};

export default ProductsListScreen

const localStyles = {
  mainContainer: { flex: 1, alignItems: 'stretch', backgroundColor: colors.backgroundPrimary }
};