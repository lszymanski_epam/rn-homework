import React, { Component } from 'react';
import { View, ScrollView, Text, Image, BackHandler, Vibration } from 'react-native';
import { styles, typography, paddings, VIBRATION_PATTERN, sentry } from '../../styles'
import { AppButton, CancelableActionModal } from '../../components'
import { createCart, addProductToCart } from '../../api'
import { Sentry, SentrySeverity } from 'react-native-sentry';

import { Notifications } from '../../nativemodules'

export class ProductScreen extends Component {

  static navigationOptions = ({navigation, screenProps}) => ({
    title: navigation.state.params.productName,
  });

  constructor(props) {
    super(props)
    this.state = {
      token: this.props.navigation.state.params.token,
      productSku: this.props.navigation.state.params.productSku,
      errorMessage: '',
      isModalVisible: false
    }
  }

  componentWillMount() {
    Notifications.initModule();
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.goBack();
    return true;
  }

  _createCart = () => {
    createCart(this.state.token)
      .then((response) => {
        if (response.errorMessage === null) {
          Notifications.showNotification(
            'Cart created',
            'Id: ' + response.result,
            () => {
              this._addProductToCart(response.result)
            })
        } else {
          Notifications.showNotification(
            'Error',
            sentry.ERROR_CREATE_CART,
            () => {
              this._sentryErrorCreateCart()
            })
          this._showErrorModal(response.errorMessage)
        }
      })
  }

  _addProductToCart = (cartId) => {
    addProductToCart(this.state.token, cartId, this.state.productSku)
      .then((response) => {
        if (response.errorMessage === null) {
          const product = response.result
          Notifications.showNotification(
            'Product has been added',
            'Name: ' + product.name,
            () => {
              this._sentryProductAddedToCart(product.id, product.name, product.sku)
            })
        } else {
          const errorMessage = 'Product could not be added to a cart id: ' + cartId
          Notifications.showNotification(
            'Error',
            errorMessage,
            () => {
              this._sentryErrorAddProductToCart(this.state,productSku, cartId)
            })
          this._showErrorModal(response.errorMessage)
        }
      })
  }

  _sentryProductAddedToCart = (productId, productName, productSku) => {
    Sentry.captureBreadcrumb({
      message: sentry.PRODUCT_ADDED_TO_CART,
      category: sentry.CATEGORY_ACTION,
      data: {
        productId: productId,
        productName: productName,
        productSku: productSku
      }
    });

    Sentry.captureMessage(sentry.PRODUCT_ADDED_TO_CART, {
      level: SentrySeverity.Debug
    });
  }

  _sentryErrorAddProductToCart = (productSku, cartId) => {
    Sentry.captureBreadcrumb({
      message: sentry.ERROR_ADD_PRODUCT_TO_CART,
      category: sentry.CATEGORY_ACTION,
      data: {
        productSku: productSku,
        cartId: cartId
      }
    });
  }

  _sentryErrorCreateCart = () => {
    Sentry.captureBreadcrumb({
      message: sentry.ERROR_CREATE_CART,
      category: sentry.CATEGORY_ACTION,
      data: {
      }
    });
  }

  _showErrorModal(message) {
    this.setState({
      errorMessage: message, 
      isModalVisible: true
    })

    Vibration.vibrate(VIBRATION_PATTERN)
  }

  _hideErrorModal() {
    this.setState({
      isModalVisible: false
    })
  }

  _onModalTryAgainActionPress = () => {
    this._createCart()
    this._hideErrorModal()
  }
  
  _onModalCancelActionPress = () => {
    this._hideErrorModal()
  }
  
  render() {
    const { goBack } = this.props.navigation;
    const { iconSource, productName } = this.props.navigation.state.params

    return (
      <View style={ localStyles.container }>
        <ScrollView contentContainerStyle={ localStyles.containerScrollView }>
          <View style={ localStyles.containerLeftHeader }>
            <Image
              style={ localStyles.logo }
              source={ iconSource }/>
            <Text 
              style={ localStyles.header }>
              { productName }
            </Text>
          </View>

          <Text 
            style={ localStyles.description }>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
          </Text>

          <View style={ localStyles.containerLeftFooter }>
            <AppButton
              title='All Products'
              onPress={ () => goBack() }
              containerStyle={ localStyles.containerButtonFirst }
              textStyle={ typography.button }/>
            <AppButton
              title='Add to cart'
              onPress={ () => this._createCart() }
              containerStyle={ localStyles.containerButtonSecond }
              textStyle={ typography.button }/>
          </View>

          <CancelableActionModal
            visible={ this.state.isModalVisible }
            title='Error'
            message= { this.state.errorMessage }
            primaryButtonText='Try again'
            onPrimaryActionPress= { () => this._onModalTryAgainActionPress() }
            onCancelActionPress= { () => this._onModalCancelActionPress() }
          />
        </ScrollView>
      </View>
    )
  }
};

export default ProductScreen

const localStyles = {
  logo: { ...styles.logo },
  header: { ...typography.textHeader },
  description: { ...typography.textRegular, marginRight: paddings.xlarge, marginLeft: paddings.standard, marginBottom: paddings.standard },
  container: { ...styles.containerProductDetails },
  containerScrollView: { paddingLeft: paddings.standard, paddingRight: paddings.standard, paddingTop: paddings.standard, paddingBottom: paddings.standard },
  containerLeftHeader: { ...styles.containerLeft, paddingBottom: paddings.standard },
  containerLeftFooter: { ...styles.containerLeft, marginLeft: paddings.standard },
  containerButtonFirst: { ...styles.buttonContainer },
  containerButtonSecond: { ...styles.buttonContainer, marginTop: paddings.small},
};