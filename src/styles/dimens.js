import React, { Component } from 'react';

export const paddings = {
  micro: 8,
  small: 24,
  medium: 32,
  large: 48,
  xlarge: 64,
  standard: 32
};

export const fontSizes = {
  small: 20,
  regular: 24,
  header: 48
};

export const dimens = {
  navbarHeight: 64
}