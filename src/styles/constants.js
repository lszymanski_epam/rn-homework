
import { Platform } from 'react-native';

const REST_API_URL = 'http://ecsc00a02fb3.epam.com/rest/V1'


export const constants = {
    LOGIN_URL: `${REST_API_URL}/integration/customer/token`,
    PRODUCT_LIST_URL: `${REST_API_URL}/products`,
    CREATE_CART_URL: `${REST_API_URL}/carts/mine`,
    ADD_PRODUCT_TO_CART_URL: `${REST_API_URL}/carts/mine/items`
};

export const storageKeys = {
    TOKEN: `token`
};

export const sentry = {
    SENTRY_DSN: 'https://ab56608f3cf74d19bf863357709aff5c:7186e5576db849009df37e6dcbb1f435@sentry.io/1396062',
    
    CATEGORY_ACTION: 'action',

    PRODUCT_SELECTED: 'Product selected from the list',
    PRODUCT_ADDED_TO_CART: 'Product added to a cart',
    ERROR_ADD_PRODUCT_TO_CART: 'Product could not be added to a cart',
    ERROR_CREATE_CART: 'Cart could not be created',

    MESSAGE_LOGGED_IN: 'User logged in'
}

export const VIBRATION_PATTERN = Platform.OS == "ios" ? [0, 500] : [0, 500, 500, 500]