import React, { Component } from 'react';

const epamPrimary = {
  blue: '#76cdd8',
  lime_green: '#cedb56',
  graphite: '#222222',
  white: '#ffffff'
};

const epamSecondary = {
  coral: '#d35d47',
  bright_blue: '#008ace',
  sharp_blue: '#39c2d7',
  dark_blue: '#263852',
  light_gray: '#cccccc',
  dark_gray: '#464547'
};


export const colors = {
  textPrimary: epamPrimary.white,
  buttonTextPrimary: epamPrimary.graphite,
  buttonBackgroundPrimary: epamPrimary.white,
  backgroundPrimary: epamPrimary.blue,
  borderPrimary: epamPrimary.graphite,
  
  textSecondary: epamSecondary.coral,
  placeholderSecondary: epamSecondary.light_gray,
  backgroundSecondary: epamSecondary.dark_blue,
  borderSecondary: epamSecondary.dark_gray,

  backgroundTertiary: epamPrimary.lime_green,

  red: '#FF0000'
};