import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { colors } from './colors'
import { fontSizes } from './dimens'

BASE_FONT = 'vincHand';

export const typography = StyleSheet.create({
  textSmall: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.small,
    color: colors.red
  },
  textRegular: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.regular,
    color: colors.textPrimary
  },
  textHeader: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.header,
    textAlign: 'center',
    color: colors.textPrimary
  },
  textNavBar: {
    flexGrow: 1,
    fontFamily: BASE_FONT,
    fontWeight: '400',
    fontSize: fontSizes.header,
    textAlign: 'center',
    color: colors.textPrimary
  },
  textModalTitle: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.header,
    textAlign: 'center',
    color: colors.textSecondary
  },
  textModalMessage: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.regular,
    textAlign: 'center',
    color: colors.textSecondary
  },
  button: {
    fontFamily: BASE_FONT,
    fontSize: fontSizes.regular,
    color: colors.buttonTextPrimary
  }
});