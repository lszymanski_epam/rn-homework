import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { colors } from './colors'
import { paddings } from './dimens'
import { typography } from './typography'


export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: colors.backgroundPrimary,
    paddingLeft: paddings.standard,
    paddingRight: paddings.standard
  },
  containerCenter: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.backgroundPrimary,
    paddingLeft: paddings.standard,
    paddingRight: paddings.standard
  },
  containerHorizontal: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.backgroundSecondary
  },
  containerLeft: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  buttonContainer: {
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.buttonBackgroundPrimary,
    borderWidth: 1,
    borderColor: colors.borderPrimary,
    borderRadius: 10,
    paddingLeft: paddings.small,
    paddingRight: paddings.small
  },
  containerCancelableActionModal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.backgroundSecondary,
    paddingLeft: paddings.standard,
    paddingRight: paddings.standard
  },
  containerProductScreen: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  containerProductItem: {
    flex: 1, 
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  containerProductDetails: {
    flex: 1, 
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: colors.backgroundPrimary
  },
  logo: {
    height: 48,
    width: 48
  },
  image: {
    height: 32,
    width: 32
  },
  animation: {
    height: 96,
    width: 96
  },
  textInput: {
    height: 48,
    textAlign: 'center',
    color: colors.textSecondary,
    backgroundColor: colors.backgroundSecondary,
    borderWidth: 1,
    borderColor: colors.borderSecondary,
    borderRadius: 10
  }
});

export const navBarStyle = {
  headerVisible: true,
  headerLeft: null,
  headerStyle: {
    backgroundColor: colors.backgroundPrimary
  },
  headerTitleStyle: typography.textNavBar,
  headerTintColor: colors.textPrimary
}