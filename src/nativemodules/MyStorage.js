import { NativeModules, AsyncStorage, Platform } from 'react-native';
import  { storageKeys } from '../styles'
import * as Keychain from 'react-native-keychain';

const nativeModule = NativeModules.MyStorage;

const isAndroid = Platform.OS === 'android'

const MySecureStorage = {
    setToken: async function(value) {
        return Keychain.setGenericPassword(storageKeys.TOKEN, value)
    },

    getToken: async function() {
        return Keychain.getGenericPassword()
    },

    clear: async function() {
        return Keychain.resetGenericPassword()
    }
}

const MyStorage = {
    setItem: async function(key, value) {
        if (isAndroid)
            return nativeModule.setItem(key, value);
        else
            return AsyncStorage.setItem(key, value);
    },

    getItem: async function(key) {
        if (isAndroid)
            return nativeModule.getItem(key);
        else
            return AsyncStorage.getItem(key);
    },

    clear: async function() {
        if (isAndroid)
            return nativeModule.clear();
        else
            return AsyncStorage.clear()
    }
}

export {
    MyStorage,
    MySecureStorage
}