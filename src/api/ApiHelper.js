import { NetInfo } from 'react-native';

const defaultHeader = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

const createHeader = (token) => {
    return { 
        ...defaultHeader, 
        'Authorization': 'Bearer ' + token
    }
}

const makeApiRequest = async (url, methodType, body = undefined, onServerErrorMessage = 'Unknown error. Please try again later.', token = '') => {
    try {
        const isInternetConnection = await NetInfo.isConnected.fetch()
        if (isInternetConnection) {
            return requestAPI(url, methodType, body, onServerErrorMessage, token)
        } else {
            let error = new Error('Device is not connected to the Internet. Please turn on Internet connection.')
            error.response = null
            throw error
        }
    } catch(error) {
        return {
            result: error.response,
            errorMessage: error.message
        }
    }
}

const requestAPI = async (url, methodType, body = undefined, onServerErrorMessage, token) => {
    return fetch(url, {
                method: methodType,
                headers: createHeader(token),
                body: body
            })
            .then((response) => checkResponseStatus(response, onServerErrorMessage))
            .then((response) => response.json())
            .then((response) => {
                return {
                    result: response,
                    errorMessage: null
                }
            })
            .catch((error) => {
                return {
                    result: error.response,
                    errorMessage: error.message
                }
            })
}

const checkResponseStatus = (response, onServerErrorMessage) => {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else if (response.status >= 400 && response.status < 500) {
        let error = new Error(onServerErrorMessage)
        error.response = response
        throw error
    } else {
        let error = new Error(response.statusText)
        error.response = response
        throw error
    }
}

const objToQueryString = (obj) => {
    const keyValuePairs = [];
    for (const key in obj) {
      keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
    }
    return keyValuePairs.join('&');
}

export {
    makeApiRequest,
    objToQueryString
};