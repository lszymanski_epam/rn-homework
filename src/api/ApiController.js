import { constants } from '../styles'
import * as ApiHelper from './ApiHelper'

const loginRequest = async (username, password) => {
    const body = JSON.stringify({
        username: username,
        password: password
    })
    return ApiHelper.makeApiRequest(
        constants.LOGIN_URL, 
        'POST', 
        body, 
        'Could not log in. Wrong email or password.',
        ''
    )
}

const productsListRequest = async (pageSize, currentPage) => {
    const queryString = ApiHelper.objToQueryString({
        'searchCriteria[pageSize]': pageSize,
        'searchCriteria[currentPage]': currentPage
    })

    return ApiHelper.makeApiRequest(
        `${constants.PRODUCT_LIST_URL}?${queryString}`, 
        'GET', 
        undefined, 
        'Could not load products. Try again later.',
        ''
    )
}

const createCart = async (token) => {
    return ApiHelper.makeApiRequest(
        constants.CREATE_CART_URL, 
        'POST', 
        undefined, 
        'Could not create a cart.',
        token
    )
}

const addProductToCart = async (token, cartId, productSku) => {
    const body = JSON.stringify({
        "cartItem": {
            "sku": productSku,
            "qty": 1,
            "quote_id": cartId
        }
    })
    return ApiHelper.makeApiRequest(
        constants.ADD_PRODUCT_TO_CART_URL, 
        'POST', 
        body, 
        'Could not create a cart.',
        token
    )
}

export {
    loginRequest,
    productsListRequest,
    createCart,
    addProductToCart
};